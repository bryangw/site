/**
 * Created by bryan_000 on 12/29/2014.
 */
var fs = require('fs');
var path = require('path');
var result = false;
var appDir = path.dirname(require.main.filename);
var root = global.appDir + path.sep + 'html' + path.sep;
function ProcessDirectory(directory) {
    fs
        .readdirSync(directory)
        .filter(function (file) {

            return (file.indexOf(".") == -1)
        })
        .forEach(function (item) {
            ProcessDirectory(directory + item);
        });

     fs
     .readdirSync(directory)
     .filter(function (file){
     return (file.indexOf(".html")> -1);
     })
     .forEach(function (item){
     AppProcessFile(directory + path.sep + item, directory);
     });
};
function AppProcessFile(file, url){
     var TemplateName =file.replace(root,'').replace(path.sep,'/');
global.Templates[TemplateName] = fs.readFileSync(file, 'utf8');

};
function LoadTemplates() {
    ProcessDirectory(root);
}

if(!global.Templates) {
    global.Templates = {};
    LoadTemplates()
}

module.exports = {Reload: LoadTemplates};