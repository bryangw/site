/**
 * Created by bryan_000 on 12/29/2014.
 */
var TemplateLoader = require('../Core/TemplateLoader');
function htmlCacheFetcher (req, res, next){
    if(global.Debug){
        TemplateLoader.Reload();
    }
    var template = req.originalUrl.replace(req.baseUrl + '/','');
    res.write(global.Templates[template]);
    res.end();
}
module.exports = htmlCacheFetcher;