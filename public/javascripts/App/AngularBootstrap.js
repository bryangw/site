/**
 * Created by bryan_000 on 1/1/2015.
 */
new function () {
    var App = window.App = angular.module('App', ['ngRoute', 'ngAnimate', 'ngSanitize']);
    App.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: '/html/Homepage/Homepage.html'
                })
                .when('/Article/:slug',
                {
                    templateUrl: '/html/Articles/Article.html',
                    controller: 'ArticleController'
                })
                .when('/Articles',
                {
                    templateUrl: '/html/Articles/ArticleList.html',
                    controller: 'ArticleListController'
                })
                .when('/Service/:slug',
                {
                    templateUrl: '/html/Articles/Article.html',
                    controller: 'ArticleController'
                })
                .when('/Us/:slug',
                {
                    templateUrl: '/html/Articles/Article.html',
                    controller: 'ArticleController'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }])
        .run(function () {
            $('.NavbarTop').css('display', '');

        });
};