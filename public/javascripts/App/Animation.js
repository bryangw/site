/**
 * Created by bryan_000 on 1/2/2015.
 */
new function() {
    window.App.animation('.View',
        ['$timeout', function($timeout) {

            var queue = {
                enter : [], leave : []
            };
            function queueAnimation(event, delay, fn) {
                var timeouts = [];
                var index = queue[event].length;
                queue[event].push(fn);
                queue[event].timer && $timeout.cancel(queue[event].timer);
                queue[event].timer = $timeout(function() {
                    angular.forEach(queue[event], function(fn, index) {
                        timeouts[index] = $timeout(fn, index * delay * 1000, false);
                    });
                    queue[event] = [];
                }, 10, false);

                return function() {
                    if(timeouts[index]) {
                        $timeout.cancel(timeouts[index]);
                    } else {
                        queue[index] = angular.noop;
                    }
                }
            };

            return {
                enter : function(element, done) {
                    if(window.isInitialized==false){
                        window.isInitialized=true;
                        return done();
                    };
                    element.css({ opacity: 0  });
                    $timeout(function () {
                        element = $(element[0]);
                        var cancel = queueAnimation('enter', 0.125, function() {

                            element.animate({ opacity : 1 }, done);
                            var cancelFn = cancel;
                            cancel = function() {
                                cancelFn();
                                element.stop();
                                element.css({ opacity : 1 });
                            };
                        });
                        return function onClose(cancelled) {
                            cancelled && cancel();
                        };
                    },300);
                },
                leave : function(element, done) {
                    element = $(element[0]);
                    var cancel = queueAnimation('leave', 0.1, function() {
                        element.css({ opacity: 1 });
                        element.animate({ opacity : 0, display:'none' }, done);
                        var cancelFn = cancel;
                        cancel = function() {
                            cancelFn();
                            element.stop();
                            //no point in animating a removed element
                        };
                    });
                    return function onClose(cancelled) {
                        cancelled && cancel();
                    };
                }
            }
        }]);
    window.App.animation('.ArticleSlide',
        [function() {

            return {
                addClass: function (element, className, done) {
                    if(className!='hideAnimation'){
                        done();
                        return function End(element,done){ };
                    }
                    $(element).find('.Article').slideUp(function (item) {
                        $(item).addClass('hidden');
                        done();
                    });
                    return function End(element,done){ };
                },
                removeClass: function (element, className, done) {

                    if(className!='hideAnimation'){
                        done();
                        return function End(element,done){ };
                    };
                    $(element).find('.Article').each(function(index,item){
                        $(item).removeClass('hidden');
                        $(item).slideUp(0).slideDown(done);
                    });
                    return function End(element,done){ };
                }
            };
        }]);
};