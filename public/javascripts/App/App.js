/**
 * Created by bryan_000 on 1/2/2015.
 */
new function () {
    var App = window.App;
    App.factory('$App', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {
        var App = this;
        $rootScope.Navigate = function (hash) {
          App.ControllerEnd();
          window.location.hash = hash;
        };
        var isInitializing = false;
        App.CheckInit = function () {
            var deferred = $q.defer();
            if (App.Data) {
                deferred.resolve();
                return deferred.promise;
            }
            $http.get('/json/Content.json')
                .success(function (data) {
                    App.Data = data;
                    deferred.resolve()
                });
            return deferred.promise;
        };
        App.PrepareNextBackground = function () {
            App.CheckInit()
                .then(function () {
                    if (App.Data.Backgrounds.length == 0) {
                        App.Data.Backgrounds = App.Data.UsedBackgrounds;
                        App.Data.UsedBackgrounds = [];
                    }
                    var rnd = Math.floor(Math.random() * App.Data.Backgrounds.length);
                    if (rnd == App.Data.Backgrounds.length) {
                        rnd--;
                    }
                    var background = App.Data.Backgrounds.splice(rnd, 1)[0];
                    App.Data.UsedBackgrounds.push(background);
                    $().Background.Prepare(background);
                });
        };
        App.GetData = function () {
            var deferred = $q.defer();
            App.CheckInit().then(function () {
                deferred.resolve(App.Data);
            });
            return deferred.promise;
        };
        App.ControllerStart = function () {
            App.PrepareNextBackground()
        };
        App.ControllerEnd = function () {
            $().Background.Switch();
        };
        App.CheckInit();
        return {
            ControllerStart: App.ControllerStart,
            ControllerEnd: App.ControllerEnd,
            GetData: App.GetData
        };
    }]);
};