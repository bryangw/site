new function () {
    var App = window.App;
    App.controller("NavbarController", ['$scope', '$App', function ($scope, $App) {
        $App.ControllerStart();
        $scope.Show = false;
        $scope.MouseEnter = function () {
            $scope.Show = true;
        };
        $scope.MouseLeave = function () {
            $scope.Show = false;
        };
    }]);
    App.controller("ArticleController", ['$scope', '$routeParams', '$App', '$sce', function ($scope, $routeParams, $App, $sce) {
        $App.ControllerStart();

        var self = this;
        self.SetArticle = function (Data) {
            $scope.Article = Data.Articles.First(function (item) {
                return item.Slug == $routeParams.slug;
            });
            $scope.Article.TrustasHTML = $sce.trustAsHtml($scope.Article.Content);
            $scope.Articles = Data.Articles;
        };

        $App.GetData().then(self.SetArticle);
    }]);
    App.controller("ArticleListPanelController", ['$scope', '$routeParams', '$App'], function ($scope, $routeParams, $App) {
        self.SetArticles = function (Data) {
            $scope.Articles = Data.Articles;
        };
        $App.GetData()
            .then(self.SetArticles);
    });
    App.controller("ArticleListPanelController", ['$scope', '$App', function ($scope, $App) {
        $App.ControllerStart();
        self.SetArticles = function (Data) {
            $scope.Articles = Data.Articles;
            defer(function () {
                $('.hideAnimation').children().addClass('hidden').slideUp()
            });
        };
        var Check = [
            {String:"Article", Section:"Articles"},
            {String:"Service", Section:"Services"},
            {String:"Us", Section:"Us"}
        ];
        $scope.SelectedSection = "";
        Check.each(function(item) {
            if ($scope.SelectedSection != '') {
                console.log("Return"); return;
            }
            if (window.location.hash.indexOf(item.String) != -1) {
                $scope.SelectedSection =  item.Section;
            }
        });

        $scope.GetMoreStyle = function (Section) {
            if (Section == $scope.SelectedSection) {
                return {'border-bottom': 'none'};
            }
        };
        $scope.MoreClick = function (Section) {
            $scope.SelectedSection = Section;
        };
        $scope.GetClass = function (Section) {
            return {'hideAnimation': !(Section == $scope.SelectedSection)}
        };
        $App.GetData()
            .then(self.SetArticles);

    }]);
};
