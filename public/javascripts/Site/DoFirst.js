/**
 * Created by bryan_000 on 12/28/2014.
 */

$(function () {
    $().Background.Initialiaze('/images/Backgrounds/Babbage.jpg');

    $('.NavbarTop').css('display','none');
    window.isInitialized=false;
});
Array.prototype.Each = function (f) {
    $(this).each(function (index,item){
       f(item);
    });
};
Array.prototype.each = Array.prototype.Each;
Array.prototype.First = function (predicate){
    var Selected = false;
    this.Each(function (item) {

        if(predicate(item)){
            Selected = item;
        }
    });
    return Selected;
};
function defer(f){
    setTimeout(f,1);
}