/**
 * Created by bryan_000 on 12/28/2014.
 */
(function ($) {

    function Initialize() {
        $('body').children().css('z-index',10)
        $('body').prepend(
            '<div class="FadingBackgroundDiv ActiveBackgroundDiv" style="position:fixed;z-index:-1;"></div>' +
            '<div class="FadingBackgroundDiv InactiveBackgroundDiv" style="position:fixed;opacity:0;z-index:-2;"></div>');

        SetSizes();
    }

    function SetSizes() {
        $(".FadingBackgroundDiv").each(function (index, item) {
            $(item).width($(window).width());
            $(item).height($(window).height());
        });
    }

    function SetBackground(selector, url) {
        var url = "url('" + window.location.protocol + '//' + window.location.host + url + "') center no-repeat";
        $(selector).css('background', url);
        $(selector).css('background-size', 'cover');
    };

    $(window).resize(SetSizes);

    $.fn.Background = {
        Initialiaze: function (url) {
            Initialize();

            SetBackground(".ActiveBackgroundDiv", url);
        },
        Prepare: function (url) {
            SetBackground('.InactiveBackgroundDiv', url);
        },
        Switch: function () {
            var ActiveBackgroundDiv = $('.ActiveBackgroundDiv');
            var InactiveBackgroundDiv = $('.InactiveBackgroundDiv');
            $(InactiveBackgroundDiv).css('z-index', -1);
            $(ActiveBackgroundDiv).css('z-index', -2);
            $(InactiveBackgroundDiv).animate({
                opacity: 1
            }, 900, function () {
                $(ActiveBackgroundDiv).removeClass('ActiveBackgroundDiv')
                    .addClass('InactiveBackgroundDiv')
                    .css('opacity', 0);
                $(InactiveBackgroundDiv).removeClass('InactiveBackgroundDiv')
                    .addClass('ActiveBackgroundDiv')


            });
        }
    };

}(jQuery));
