var express = require('express');
var router = express.Router();

function GetPartials(){
  return {
    Navbar: global.Templates['Layout/NavBar.html'],
    Homepage: global.Templates['Homepage/Homepage.html']
  }
}
/* GET home page. */
router.get('/', function(req, res) {
  //var Template = Hogan.compile()
  res.render('index', GetPartials());
});

module.exports = router;
